/*
 * Copyright (c) 2019-2021, JetBrains s.r.o. and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. JetBrains designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact JetBrains, Na Hrebenech II 1718/10, Prague, 14000, Czech Republic
 * if you need additional information or have any questions.
 */
@file:Suppress("JAVA_MODULE_DOES_NOT_EXPORT_PACKAGE")

package org.jetbrains.projector.server.service

import org.jetbrains.projector.awt.image.PVolatileImage
import org.jetbrains.projector.awt.service.ImageCacher
import org.jetbrains.projector.common.protocol.data.ImageData
import org.jetbrains.projector.common.protocol.data.ImageId
import org.jetbrains.projector.common.protocol.toClient.ServerImageDataReplyEvent
import org.jetbrains.projector.server.ProjectorServer
import org.jetbrains.projector.server.core.util.SizeAware
import org.jetbrains.projector.util.loading.unprotect
import org.jetbrains.projector.util.logging.Logger
import sun.awt.image.SunVolatileImage
import sun.awt.image.ToolkitImage
import sun.java2d.StateTrackable
import java.awt.Image
import java.awt.image.*
import java.io.ByteArrayOutputStream
import java.lang.ref.SoftReference
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import javax.imageio.ImageIO

/**
 * Projector 的图片缓存器
 *
 * 每次有新的图片 draw 的时候，Server 会添加到 newImages 中，主动增量 ServerImageDataReplyEvent 推给客户端。
 * 客户端在渲染的时候，发现图片早不到时，会主动向服务端发起 ClientRequestImageDataEvent 获取，可断点 getImage 方法。
 * 客户端与服务端，通过 ImageId 来作为图片的唯一标识
 */
object ProjectorImageCacher : ImageCacher {

  /**
   * 获得图片对应的 ImageId 对象
   *
   * @param image 图片
   * @param methodName 用于调试排查问题的方法名，实际没啥用
   * @return ImageId 对象，标识图片
   */
  override fun getImageId(image: Image, methodName: String): ImageId = when (image) {
    // 添加图片，根据情况返回给客户端
    is BufferedImage -> putImage(image)

    is ToolkitImage -> getImageId(image.bufferedImage, "$methodName, extracted BufferedImage from ToolkitImage")

    is PVolatileImage -> ImageId.PVolatileImageId(image.id)

    is SunVolatileImage -> getImageId(image.snapshot, "$methodName, extracted snapshot from SunVolatileImage")

    is MultiResolutionImage -> image.resolutionVariants
                                 .singleOrNull()
                                 ?.let { getImageId(it, "$methodName, extracted single variant") }
                               ?: ImageId.Unknown(
                                 "$methodName received MultiResolutionImage with bad variant count (${image.resolutionVariants.size}): $image")

    else -> ImageId.Unknown("$methodName received ${image::class.qualifiedName}: $image")
  }

  /**
   * 新图片的队列
   * 会在每 10 毫秒 UpdateThread 推送给客户端。发送完，会进行清理。
   */
  val newImages by SizeAware(
    ConcurrentLinkedQueue<ServerImageDataReplyEvent>(),
    if (ProjectorServer.ENABLE_BIG_COLLECTIONS_CHECKS) ProjectorServer.BIG_COLLECTIONS_CHECKS_START_SIZE else null,
    Logger<ProjectorImageCacher>(),
  )

  private data class LivingImage(val reference: SoftReference<Image>, val data: ImageData)

  private data class IdentityImageId(val identityHash: Int, val stateHash: Int)

  /**
   * ImageId =》LivingImage 的映射，包含图片
   */
  private var idToImage = mutableMapOf<ImageId, LivingImage>()

  /**
   * IdentityImageId =》ImageId 的映射
   * IdentityImageId：图片的身份标识，用于身份标识
   * ImageId：序列化后的图片编号，用于网络通讯
   */
  private val identityIdToImageId = mutableMapOf<IdentityImageId, ImageId>()

  private fun <T : Image> putImageIfNeeded(
    identityImageId: IdentityImageId,
    image: T,
    imageIdBuilder: T.() -> ImageId,
    imageConverter: T.() -> ImageData,
  ) {
    synchronized(this) {
      if (identityImageId !in identityIdToImageId) {
        // 构建 identityIdToImageId 的映射
        val imageId = image.imageIdBuilder()
        identityIdToImageId[identityImageId] = imageId
        // 构建 idToImage 的映射
        if (imageId !in idToImage) {
          val imageData = image.imageConverter()
          idToImage[imageId] = LivingImage(SoftReference(image), imageData)
          // 添加到 newImages 队列中
          newImages.add(ServerImageDataReplyEvent(imageId, imageData))
        }
      }
    }
  }

  fun putImage(image: BufferedImage): ImageId {
    // 生成 IdentityImageId 对象
    val id = IdentityImageId(
      identityHash = System.identityHashCode(image),
      stateHash = image.stateHash
    )
    // 根据情况，添加到图片缓存。重要的是 newImages，可以推送增量的图片给客户端
    putImageIfNeeded(id, image, BufferedImage::imageId, BufferedImage::toImageData)
    // 返回 ImageId 用于通讯
    return identityIdToImageId[id]!!
  }

  fun getImage(id: ImageId): ImageData? {
    return idToImage[id]?.data
  }

  /**
   * 手动垃圾回收。
   * 因为，idToImage 使用的软连接，所以 identityIdToImageId 可能对应的图片不存在
   */
  fun collectGarbage() {
    synchronized(this) {
      filterNullsOutOfMutableMap(idToImage)
      identityIdToImageId.removeAllImageIdsWithoutImages()
    }
  }

  /**
   * 判断软连接，是否存活
   */
  private fun <K> isAlive(entry: Map.Entry<K, LivingImage>): Boolean {
    return entry.value.reference.get() != null
  }

  /**
   * 回收 idToImage
   */
  private fun <K> filterNullsOutOfMutableMap(map: MutableMap<K, LivingImage>) {
    val iterator = map.iterator()

    while (iterator.hasNext()) {
      val next = iterator.next()

      if (!isAlive(next)) {
        iterator.remove()
      }
    }
  }

  /**
   * 回收 identityIdToImageId
   */
  private fun <K> MutableMap<K, ImageId>.removeAllImageIdsWithoutImages() {
    val iterator = iterator()

    while (iterator.hasNext()) {
      val next = iterator.next()

      if (next.value !in idToImage) {
        iterator.remove()
      }
    }
  }
}

private fun BufferedImage.toPngBase64(): String {
  val imageInByte: ByteArray

  ByteArrayOutputStream().apply {
    ImageIO.write(this@toPngBase64, "png", this)
    this.flush()
    imageInByte = this.toByteArray()
    this.close()
  }

  val encoded = Base64.getEncoder().encode(imageInByte)

  return String(encoded)
}

fun BufferedImage.toImageData(): ImageData {
  return ImageData.PngBase64(this.toPngBase64())
}

private val dataFieldByte = DataBufferByte::class.java.getDeclaredField("data").apply {
  unprotect()
}

private val dataFieldInt = DataBufferInt::class.java.getDeclaredField("data").apply {
  unprotect()
}

val BufferedImage.imageId: ImageId
  get() = when (raster.dataBuffer) {
    is DataBufferByte -> {
      val pixels = dataFieldByte.get(raster.dataBuffer) as ByteArray

      ImageId.BufferedImageId(
        rasterDataBufferSize = pixels.size,
        contentHash = pixels.contentHashCode()
      )
    }
    is DataBufferInt -> {
      val pixels = dataFieldInt.get(raster.dataBuffer) as IntArray

      ImageId.BufferedImageId(
        rasterDataBufferSize = pixels.size,
        contentHash = pixels.contentHashCode()
      )
    }
    else -> error("Unsupported BufferedImage type")
  }

private val theTrackableField = DataBuffer::class.java.getDeclaredField("theTrackable").apply {
  unprotect()
}

val BufferedImage.stateHash
  get(): Int {
    val stateTrackable = theTrackableField.get(this.raster.dataBuffer) as StateTrackable
    val stateTracker = stateTrackable.stateTracker

    return System.identityHashCode(stateTracker)
  }
