/*
 * Copyright (c) 2019-2021, JetBrains s.r.o. and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation. JetBrains designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact JetBrains, Na Hrebenech II 1718/10, Prague, 14000, Czech Republic
 * if you need additional information or have any questions.
 */
@file:Suppress("JAVA_MODULE_DOES_NOT_EXPORT_PACKAGE")

package org.jetbrains.projector.awt.image

import org.jetbrains.projector.awt.PWindow
import org.jetbrains.projector.awt.font.PFontManager
import sun.awt.image.BufImgSurfaceData
import sun.java2d.SunGraphics2D
import sun.java2d.SunGraphicsEnvironment
import java.awt.*
import java.awt.image.BufferedImage
import java.util.*

/**
 * Projector 自定义的 GraphicsEnvironment 图形环境
 * 主要用于创建自定义的 PGraphicsDevice 图形设备
 */
class PGraphicsEnvironment : SunGraphicsEnvironment() {

  companion object {
    var clientDoesWindowManagement: Boolean = false

    /**
     * 设备列表
     */
    val devices = ArrayList<PGraphicsDevice>().apply { add(PGraphicsDevice("Screen0")) }

    // this is questionable from garbage collection point of view, but given that normally at most two instances of PGE should be created, it's not that bad
    private val instances = ArrayList<PGraphicsEnvironment>()

    /**
     * 默认 PGraphicsDevice 设备，下面 getDefaultScreenDevice 方法会使用
     */
    val defaultDevice: PGraphicsDevice
      get() = devices[0]

    /**
     * 基于新的设备，修改展示
     */
    fun setupDisplays(additionalDisplays: List<Pair<Rectangle, Double>>) {
      // 清空原有设备
      devices.clear()
      // 设置新的设备
      additionalDisplays.forEachIndexed { i, it ->
        val element = PGraphicsDevice("Screen$i")
        element.bounds.bounds = it.first
        element.scaleFactor = it.second
        devices.add(element)
      }
      // 设备变化，所以触发展示变化
      fireDisplaysChanged()
    }

    private fun fireDisplaysChanged() {
      instances.forEach { it.displayChanged() }
      PWindow.windows.forEach { it.updateGraphics() }
    }
  }

  init {
    instances.add(this)
  }

  val xResolution: Double = 96.0
  val yResolution: Double = 96.0

  override fun getScreenDevices(): Array<GraphicsDevice> {
    return devices.toTypedArray()
  }

  override fun getDefaultScreenDevice(): GraphicsDevice {
    return defaultDevice
  }

  override fun createGraphics(img: BufferedImage): Graphics2D {
    // TODO: will it work without hardware acceleration?
    return SunGraphics2D(BufImgSurfaceData.createData(img), Color.WHITE, Color.WHITE, PFontManager.allInstalledFonts.first())
  }

  override fun getAllFonts(): Array<Font> {
    return PFontManager.allInstalledFonts
  }

  override fun getAvailableFontFamilyNames(): Array<String> {
    return getAvailableFontFamilyNames(Locale.getDefault())
  }

  override fun getAvailableFontFamilyNames(requestedLocale: Locale): Array<String> {
    return PFontManager.getInstalledFontFamilyNames(requestedLocale)
  }

  fun setDefaultDeviceSize(width: Int, height: Int) {
    defaultDevice.bounds.setSize(width, height)
    fireDisplaysChanged()
  }

  override fun getNumScreens(): Int = devices.size

  override fun isDisplayLocal(): Boolean = false

  override fun makeScreenDevice(p0: Int): GraphicsDevice = devices[p0]
}
